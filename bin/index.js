#! /usr/bin/env node
const program = require("commander");
const chalk = require('chalk')
const createtor = require("../lib/create.js")
const pkg = require('../package.json')

console.log(
chalk.hex("#ff8800").bold(`
╭┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈╮
┆                                     ┆
┆        ${chalk.green("Welcome to the sweet")}         ┆
┆             ${chalk.red("vtmp-cli")}                ┆
┆           ${chalk.green(`version: ${pkg.version}`)}            ┆
┆  🚀 🚀 🚀                           ┆
╰┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈╯
`)
);

program
  .version(`${pkg.version}`)
  .option("-f, --force", "overwrite target directory if it exist") // 是否强制创建，当文件夹已经存在
  .command("create <app-name>")
  .description("create a new project")
  .action((name, options) => {
    // 在 create.js 中执行创建任务
    createtor(name, options);
  });

program.parse(process.argv);
