<!--
 * @Descripttion: 
 * @version: 
 * @Author: chunwen
 * @Date: 2022-04-15 18:04:07
 * @LastEditors: 
 * @LastEditTime: 2022-04-15 18:33:11
-->
## How to use

1. install dependence, you can use cnpm or yarn in China

   ```
   npm install
   ```

   

2. run in the development

   ```
   npm run dev 或 npm run start
   ```

   

3. build in production

   ```
   npm run build
   ```
