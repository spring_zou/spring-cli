<!--
 * @Descripttion: 
 * @version: 
 * @Author: chunwen
 * @Date: 2022-04-18 22:58:57
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-09 16:26:23
-->

# vtmp-cli
## Installation

```
npm i vuetmp-cli -g
```

### Then generate your new project:
```
vtmp-cli create xxx
```

### 强制新建创建项目:
```
vtmp-cli create xxx -f
```

### 发布
```
npm publish
```

> 如果想改造成执行命令拉取git仓库资源，可以使用`download-git-repo`工具来实行
https://www.npmjs.com/package/download-git-repo
