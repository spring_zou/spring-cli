const path = require('path')
const fs = require("fs-extra");
const chalk = require("chalk");
const Generator = require('./build')

// 询问用户输入
const inquirer = require('inquirer')

module.exports = async (name, options) => {
  const cwd = process.cwd();
  const destDir = path.join(cwd, name);

  if (fs.existsSync(destDir)) {
    // 是否强制创建
    if (options.force) {
      await fs.remove(destDir);
    } else {
      // 询问用户是否确定要覆盖
      let { action } = await inquirer.prompt([
        {
          name: "action",
          type: "list",
          message: "Target directory already exists Pick an action:",
          choices: [
            {
              name: "Overwrite",
              value: "overwrite",
            },
            {
              name: "Cancel",
              value: false,
            },
          ],
        },
      ]);
      if (!action) {
        return;
      } else if (action === "overwrite") {
        // 移除已存在的目录
        console.log(chalk.hex("#ff8800").bold(`\r\nRemoving...`));
        await fs.remove(destDir);
        console.log(chalk.hex("#ff8800").bold(`\r\ninquirer start...`));
      }
    }
  }

  // 执行创建
  const createtor = new Generator(name, destDir);
  createtor.create()
};


